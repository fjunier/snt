---
author: Frédéric Junier
title: Algorithmes et programmes
---

# Algorithmes et programmes

!!! abstract "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le parcours thématique Photographie numérique proposé par l'association [France IOI](https://www.france-ioi.org/) sur le site [https://parcours.algorea.org](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025/)
    * le cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/) 
    * des TP sur Capytale de David Landry et Germain Becker.



## Traitement d'image numérique

!!! question "Exercice 1"
    > **Objectif :** _Sélectionner des zones de pixels pour passer au négatif d'une image_

    1. Se connecter à son compte sur [parcours-algorea.org](https://login.france-ioi.org/auth){:target="_blank"}.
    2. Dans le le parcours thématique _Photographie numérique_, faire l'exercice [Rectangles inversés](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025-1089626022569595539-174905765139091083/){:target="_blank"}.


!!! question "Exercice 2"
    > **Objectif :** _Découvrir la notion de  calques dans un logiciel de traitement d'images_

    1. Se connecter à son compte sur [parcours-algorea.org](https://login.france-ioi.org/auth){:target="_blank"}.
    2. Dans le parcours thématique _Photographie numérique_, faire l'exercice [Autocollants](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025-1089626022569595539-390677530328629986/){:target="_blank"}.
    3.  Compléter le texte à trous du  [Quiz - Traitement d'images](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025-1089626022569595539-309540271462831363/){:target="_blank"}.


!!! note "Point de cours 5"

    === "Repérage des pixels"
        Dans une image bitmap, chaque pixel est repéré par son abscisse et son ordonnée dans un repère dont l'origine est le coin supérieur gauche de l'image. L'axe des abscisses est orienté vers la droite et l'axe des ordonnées vers le bas.

        ![alt](https://cgouygou.github.io/2SNT/Cours/images/tabpixels.png)

        > _Source : [site de Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/#2-limage-numerique)_

    ===  "Traitement des pixels"
        Un algorithme de traitement d'image parcourt la matrice de pixels et applique pour chaque pixel une transformation :

        * il peut _modifier_ la valeur du pixel (négatif d'une image par exemple)
        * il peut aussi _déplacer_ le pixel (flop d'une image par exemple)


        

        !!! example "Négatif d'une image"
            Le *négatif* d'une image bitmap est un traitement d'image qui associe à chaque pixel sa valeur complémentaire. 
            
            |Type d'image|Valeur du pixel|Négatif du pixel|
            |:---:|:---:|:---:|
            |Noir et blanc| $0 \leqslant v \leqslant 1$|$1-v$|
            |Niveaux de gris| $0 \leqslant v \leqslant 255$|$255-v$|
            |Couleur | $(r, v, b)$ avec  $0 \leqslant r, v ,b \leqslant 255$|$(255-r, 255- v, 255 -b)$|

            L'algorithme est simple : on parcourt la matrice de pixels de l'image avec deux boucles imbriquées sur les abscisses et les ordonnées  et on inverse chaque pixel.

           
                Pour chaque abscisse x
                    Pour chaque ordonnée y
                        pixel en (x, y) prend pour valeur sa négation
            

            === "image initiale en niveaux de gris"
                ![alt](images/mandrill_gris.png)

            === "négatif"
                ![alt](images/mandrill_negate.png)

        !!! example "Flop d'une image"
            Le *flop*  d'une image bitmap est un traitement d'image qui déplace chaque pixel vers une nouvelle position qui est sa symétrique par rapport à l'un des bords verticaux de l'image.  

            L'algorithme est simple : on parcourt  la matrice de pixels de l'image avec deux boucles imbriquées sur les abscisses et les ordonnées  et on déplace  chaque pixel. Cependant il faut créer une nouvelle image.

                Création d'une  matrice de pixels d'une nouvelle image de mêmes dimensions 
                Pour chaque abscisse x
                    Pour chaque ordonnée y
                        On affecte la valeur du pixel en (x, y) de l'image au pixel symétrique en (largeur - x, y) dans la nouvelle image
            

            === "image initiale"
                ![alt](images/drapeau-france.png)

            === "flop"
                ![alt](images/drapeau-flop.png)

## Traitement d'image avec Python



!!! question "Exercice 3"
    Deux TP sont proposés, j'ai repris les excellents TP de David Landry qu'il a partagés sur [Capytale](https://capytale2.ac-paris.fr/){:target="_blank"} :

    * _Objectif : créer une image numérique, découvrir les fonctions de manipulation de pixels_ : [💻 TP 1 sur Capytale](https://capytale2.ac-paris.fr/web/c/f5a7-3798315){:target="_blank"}. 
    * _Objectif : traitement plus avancé d'images, parcours de matrice de pixels et sélection de zones de pixels_ :  [💻 TP 2 sur Capytale](https://capytale2.ac-paris.fr/web/c/0dcd-3798316){:target="_blank"}.
