---
author: Frédéric Junier
title: Impact sur les pratiques humaines
---

#  Impact sur les pratiques humaines

!!! abstract "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le parcours thématique Photographie numérique proposé par l'association [France IOI](https://www.france-ioi.org/) sur le site [https://parcours.algorea.org](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025/)
    * le cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/) 
    * des TP sur Capytale de David Landry et Germain Becker.


!!! note "Point de cours 6"
    === "Nouveaux usages"
        La photographie numérique a transformé nos pratiques  avec de nouveaux usages :

          - Partage instantané sur les réseaux sociaux
          - Photographie utilitaire pour mémoriser des informations (ex. : tickets, notes)
  
        Le développement des techniques de reconnaissance et de création d'images basées sur l'Intelligence Artificielle  bouleverse de nombreux secteurs (sécurité, imagerie médicale, analyse d'images aériennes pour repérer les piscines non déclarées à l'administration fiscale, voitures autonomes ...)

    === "Questions juridiques et  éthiques" 
        - Problèmes liés à la diffusion incontrôlée des images et au respect de la vie privée
        - Droit à l'oubli numérique
        - Les métadonnées intégrées aux photos peuvent révéler des informations personnelles
        - Défis de l'archivage des photographies numériques historiques et culturelles
        - Défis de la détection d'images générées par des outils d'Intelligence Artificielle 

!!! question "Exercice 1"

    Traiter ce [💻 TP 3 sur Capytale](https://capytale2.ac-paris.fr/web/c/d622-3798261) du projet [MathaData](https://mathadata.fr/fr){:target="_blank"} qui permet d'aborder le domaine de la reconnaissance d'image à travers une technique de classification d'image à l'aide de statistiques sur les valeurs de ses pixels.
