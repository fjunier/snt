---
author: Frédéric Junier
title: Introduction
---


!!! cite "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le parcours thématique Photographie numérique proposé par l'association [France IOI](https://www.france-ioi.org/) sur le site [https://parcours.algorea.org](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025/)
    * le cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/) 
    * des TP sur Capytale de David Landry et Germain Becker.


!!! question "Activité d'introduction"

    <iframe width="1098" height="617" src="https://www.youtube.com/embed/UnNPNc-F9ks" title="MOOC SNT / Photographie numérique, du réel aux pixels ?" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

    !!! success "Question 1"
        Regardez la vidéo précédente.
    
    !!! success "Question 2"
        1. Si ce n'est déjà fait, se créer un compte sur [parcours.algorea.org](https://login.france-ioi.org/register){:target="_blank"} puis se connecter et rejoindre le groupe SNT-Parc-2024 depuis [https://parcours.algorea.org/profile/groupsMember](https://old.parcours.algorea.org/profile/groupsMember){:target="_blank"} avec le code donné dans Pronote ou par le professeur.
        2. Répondre au [Quizz](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025-1089626022569595539-1943397278515775575/){:target="_blank"} sur la vidéo précédente.
