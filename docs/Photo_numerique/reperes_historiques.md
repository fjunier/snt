---
author: Frédéric Junier
title: Repères historiques
---


!!! abstract "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le parcours thématique Photographie numérique proposé par l'association [France IOI](https://www.france-ioi.org/) sur le site [https://parcours.algorea.org](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025/)
    * le cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/) 
    * des TP sur Capytale de David Landry et Germain Becker.


!!! note "Point de cours 1 : repères historiques"

    <iframe src='https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=1Z7fH0edjGmOx2TZdtWadzLzf2AImRvHxrjjyzprqHV8&font=Default&lang=en&initial_zoom=2&height=650' width='100%' height='650' webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder='0'></iframe>

