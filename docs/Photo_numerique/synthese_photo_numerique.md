---
author: Frédéric Junier
title: SNT synthèse  photographie numérique
---

# SNT synthèse  photographie numérique

> [Version pdf](./synthese_photo_numerique.pdf)

## Repères historiques

* _1827_ : naissance de la photographie argentique  
* _1903_ : photographie en couleurs. Après la seconde guerre mondiale, généralisation du format 24x36 et de la visée reflex ;
* _1969_ : arrivée des premiers capteurs CCD (Charge Coupled Device) ;
* _1975_ : apparition des premiers appareils numériques ;
* _2007_ : arrivée du smartphone.

## Données et informations

!!! note "Point de cours 1"

    !!! abstract "Image bitmap"

        Pour représenter numériquement une image on la découpe en une **grille** ou **matrice** de **pixels** (_picture element_) et on associe à chaque pixel une valeur numérique. On parle de **représentation bitmap** (_matrice de bits_), les bits permettant de stocker les valeurs des pixels.  

        !!! example "une image en noir et blanc"
            Ci-dessous la représentation en noir et blanc d'une image de poisson dans une matrice de pixels. Chaque pixel vaut 1 s'il est _blanc_ ou 0 s'il est _noir_.

            ![alt](https://static-items.algorea.org/files/checkouts/02a3e908c759556ec457ca36b990bc68/digital_photo/structuration_digital_photo_1/size.png)

            > **Image 1**
            > _Source :_ [https://parcours.algorea.org/](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025-1089626022569595539-349914841333683613/)


    !!! abstract "Définition et résolution d'une image"
        
        La **définition** d'une image est le nombre de pixels qui composent l'image.

        La **résolution** d'une image est le nombre de pixels par unité de longueur. On l'exprime en général en ppp(_pixels par pouce_  ou _dots per inch (dpi)_ en anglais, le pouce est une unité de longueur anglo-saxonne mesurant $2,54$ cm. La résolution standard pour le Web est de 72 ppp et pour une impression de 300 ppp. 

        

        !!! example "calcul de la définition et de la résolution d'une image"
            L'image précédente comporte 12 pixels en largeur et 5 pixels en hauteur donc sa définition est de $5 \times 12 = 60$ pixels.
            Cette image a une faible résolution de 3 pixels par pouce.



!!! note "Point de cours 2"

    !!! abstract "Unités d'information"
        En informatique, l'unité d'information de base est le **bit** (_binary digit_) qui permet de stocker 2 informations (codées par 0 ou 1).
        Un **octet** (_byte_ qui signifie morceau en anglais) est une séquence de 8 bits et permet de stocker $2^{8}=256$ informations (codées par une séquence de 8 valeurs 0 ou 1).

    !!! abstract "Profondeur d'une image bitmap"
        La **profondeur** d'une image numérique bitmap, est le nombre de bits utilisé par chaque pixel pour coder sa couleur.

        Avec une profondeur de $p$ bits on peut coder $2^{p}$ couleurs différentes.

    !!! abstract "Poids d'une image"

        A partir de la **définition** et de la **pronfondeur** d'une image bitmap on peut calculer son poids avec la formule : **Poids $=$ Définition $\times$ Profondeur**.

        
        !!! example "Calcul du poids d'une image bitmap"

            Pour une image en noir et blanc, un pixel a deux couleurs possibles. Comme $2=2^{1}$,  il suffit de 1 bit pour coder sa couleur, sa profondeur est de 1 bit.

            Par exemple l'image 1  de définition $12 \times 5 = 60$ et de profondeur $1$ bit a pour poids $60 \times 1= 60$ bits.

           
    !!! abstract "Différentes profondeurs d'images"

        La profondeur d'une image peut varier selon l'échelle des couleurs choisies :

        * pour une image en _noir et blanc_ une profondeur de 1 bit suffit pour représenter $2^{1}$ couleurs
        * pour une image en _niveaux de gris_ une profondeur de 8 bits soit 1 octet  permet de  représenter $2^{8}=256$ nuances de gris. La valeur du pixel représente l'intensité lumineuse donc les nuances de gris s'échelonnent  du noir ($0$) au blanc ($255$)
        * pour une image en _couleurs_ on utilise la **synthèse additive** des couleurs en représentant chaque couleur comme l'addition de trois intensités lumineuses de *Rouge*, de *Vert* et de *Bleu*. La couleur de chaque pixel est donc codée par un triplet **(R, V, B)**. Si chaque composante de ce triplet est codée sur $8$ bits soit $1$ octet cela donne une profondeur de $8 \times 3 = 24$ bits qui permet de représenter $2^{24} \approx 16 \times 10^{6}$ couleurs. 


        ![alt](https://cgouygou.github.io/2SNT/Cours/images/chromato.jpg){:.center}

        > _Source :   cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/)_

        > Exemples de codages (R, G, B) de pixels d'une image en couleur. Une couleur dont les 3 composantes sont identiques correspond à un niveau de gris.



!!! note "Point de cours 3"

    !!! abstract "Format de fichier"
        Un **format de fichier** d'image numérique est une façon d'encoder les informations de l'image numérique dans un fichier informatique qui est une séquence de bits. 
        On distingue :
        
        * les **fichiers textuels** dont la séquence de bits correspond à une séquence de caractères lisibles par l'être humain
        * les **fichiers binaires** dont la séquence de bits ne correspond pas à une séquence de caractères lisibles.
  
   
        Il existe plusieurs formats de fichiers binaires pour les images numériques. Ils se distinguent principalement selon trois critères :


        | Nom du format et extension | Profondeur (nombre de bits par pixel) | Type de compression (avec ou sans perte) | Exemples d'utilisation                       |
        |---------------|---------------------------------------|------------------------------------------|---------------------------------------------|
        | PNG           | 24 bits (Truecolor), 8 bits (Indexed) | Sans perte                               | Logos, illustrations avec transparence, web |
        | JPEG          | 24 bits (Truecolor)                   | Avec perte                               | Photos haute résolution, web                |
        | BMP           | 1, 4, 8, 16, 24, 32 bits              | Sans perte                               | Captures d'écran, images non compressées    |
        | TIFF          | 1, 8, 24, 48 bits                     | Sans perte ou avec perte                 | Imagerie médicale, archivage, impression    |
        | RAW           | 12 à 16 bits                          | Sans perte                               | Photographie professionnelle                |
        | GIF           | 8 bits                                | Sans perte                               | Animations simples, graphiques web          |

       

    !!! abstract "Métadonnées d'un fichier"
        Les **métadonnées** d'un fichier d'image numérique sont des informations complémentaires  à la représentation de l'image comme l'origine (date localisation géographique), la création (l'auteur), les caractéristiques (techniques de l'appareil) qui  permettent de mieux comprendre l'image et de la classer dans une collection d'images.  
        Les métadonnées des images numériques prises par les appareils photo numériques suivent en général la norme [EXIF](https://fr.wikipedia.org/wiki/Exchangeable_image_file_format).
        Il faut être prudent dans la manipulation des **métadonnées** qui peuvent contenir des données personnelles.



## Machines


!!! note "Point de cours 4"
    !!! abstract "Capteur CCD"
        Le composant de base d'un appareil de photographie numérique est le **capteur CCD**   (_Charge Coupled Device_) , inventés en 1969. Ils  capturent la lumière à travers des **photosites** (diamètre de l'ordre du micromètre), la convertissent en charges électriques avec **l'effet photoélectique**. Ces charges sont trasnsférées jusqu'à un **convertisseur analogique/numérique**  qui génère un signal numérique (séquence de 0 et de 1) pour créer une image numérique.


    !!! abstract "Importance des algorithmes"
        Les algorithmes jouent un rôle crucial dans la photographie numérique  et sont appliqués avant la prise de vue ou sur les données brutes au format [RAW](https://fr.wikipedia.org/wiki/RAW_%28format_d%27image%29)  fournies par le  capteur CCD :

        - **Prise de vue** :  calcul de l'exposition, mise au point automatique, stabilisation d'image
        - **Développement** : balance des blancs, correction des distorsions, ajustement de la netteté et du contraste
        - **Compression de fichiers** : compression sans perte (ex. : format TIFF), compression avec perte (ex. : format JPEG)



!!! note "Point de cours 5"

    !!! abstract "Repérage des pixels"
        Dans une image bitmap, chaque pixel est repéré par son abscisse et son ordonnée dans un repère dont l'origine est le coin supérieur gauche de l'image. L'axe des abscisses est orienté vers la droite et l'axe des ordonnées vers le bas.

        ![alt](https://cgouygou.github.io/2SNT/Cours/images/tabpixels.png)

        > _Source : [site de Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/##2-limage-numerique)_

    !!! abstract  "Traitement des pixels"
        Un algorithme de traitement d'image parcourt la matrice de pixels et applique pour chaque pixel une transformation :

        * il peut _modifier_ la valeur du pixel (négatif d'une image par exemple)
        * il peut aussi _déplacer_ le pixel (flop d'une image par exemple)


## Impact sur les pratiques humaines

!!! note "Point de cours 6"
    !!! abstract "Nouveaux usages"
        La photographie numérique a transformé nos pratiques  avec de nouveaux usages :

          - Partage instantané sur les réseaux sociaux
          - Photographie utilitaire pour mémoriser des informations (ex. : tickets, notes)
  
        Le développement des techniques de reconnaissance et de création d'images basées sur l'Intelligence Artificielle  bouleverse de nombreux secteurs (sécurité, imagerie médicale, analyse d'images aériennes pour repérer les piscines non déclarées à l'administration fiscale, voitures autonomes ...)

    !!! abstract "Questions juridiques et  éthiques" 
        - Problèmes liés à la diffusion incontrôlée des images et au respect de la vie privée
        - Droit à l'oubli numérique
        - Les métadonnées intégrées aux photos peuvent révéler des informations personnelles
        - Défis de l'archivage des photographies numériques historiques et culturelles
        - Défis de la détection d'images générées par des outils d'Intelligence Artificielle 