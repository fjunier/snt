---
author: Frédéric Junier
title: Machines
---

# Machines


!!! abstract "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le parcours thématique Photographie numérique proposé par l'association [France IOI](https://www.france-ioi.org/) sur le site [https://parcours.algorea.org](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025/)
    * le cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/) 
    * des TP sur Capytale de David Landry et Germain Becker.

## Capteurs CCD

!!! question "Exercice 1"

    !!! success "Question 1"

        === "énoncé"
            Dans un nouvel onglet, interroger [l'agent conversationnel ChatGPT](https://chatgpt.com)  à propos du fonctionnement des capteurs CCD. Construire soigneusement le _prompt_ en précisant :

            * votre statut : élève de lycée
            * le sien : un professeur
            * votre objectif : apprendre le fonctionnement des capteurs CCD (_Charge Coupled Device_) d'un appareil de photographie numérique
            * des contraintes sur sa réponse :   se concentrer sur quatre notions clefs.
        
        === "solution"

            Voici mon _prompt_ :

            > Je suis un élève de lycée. Tu es un professeur de sciences numériques et technologie. Tu dois m'expliquer le fonctionnement des capteurs CCD d'un appareil de photographie numérique. Tu dois te concentrer sur quatre notions essentielles.
        

            Voici une réponse du chatbot  (avec le même prompt vous n'aurez pas exactement la même réponse):

            ??? example "réponse du chatbot"
                Absolument ! En tant que professeur de sciences numériques et technologie, je suis ravi de t'aider à comprendre le fonctionnement des capteurs CCD, un élément fondamental des appareils photo numériques.

                Concentrons-nous sur ces quatre notions essentielles :

                1. Qu'est-ce qu'un capteur CCD ?

                    Imagine un capteur CCD comme une immense grille composée de millions de petites cellules photosensibles, appelées photosites. Chaque photosite est capable de capturer la lumière et de la convertir en un signal électrique. C'est un peu comme si chaque photosite était un petit œil qui enregistre une partie de l'image.

                2. Comment fonctionne la capture de la lumière ?

                    Lorsque tu prends une photo, la lumière traverse l'objectif et vient frapper le capteur CCD. Chaque photosite absorbe une certaine quantité de lumière et génère une charge électrique proportionnelle à l'intensité lumineuse reçue. Plus la lumière est intense, plus la charge électrique est importante.
                    
                3. Le transfert de charge : le cœur du CCD

                    Le nom "CCD" signifie "dispositif à transfert de charges". Une fois que chaque photosite a accumulé sa charge électrique, celle-ci est transférée d'un photosite à l'autre, un peu comme une vague qui se déplace sur l'océan. Ce transfert de charge permet de rassembler toutes les informations électriques générées par les photosites et de les envoyer vers un convertisseur analogique-numérique.
                    
                4. La conversion en image numérique

                    Le convertisseur analogique-numérique transforme le signal électrique analogique provenant du capteur CCD en un signal numérique. Ce signal numérique est constitué d'une multitude de valeurs numériques, chacune correspondant à la luminosité d'un pixel de l'image finale. C'est ce signal numérique qui sera ensuite traité par l'appareil photo et enregistré en tant qu'image.

                **Pour résumer :**

                - **Capture de la lumière :** Les photosites convertissent la lumière en charges électriques.
                - **Transfert de charge :** Les charges électriques sont déplacées d'un photosite à l'autre.
                - **Conversion analogique-numérique :** Le signal électrique est transformé en un signal numérique.
                - **Image numérique :** Le signal numérique est interprété comme une image.

                 En résumé, un capteur CCD est comme un œil électronique qui décompose la lumière en une multitude de points lumineux, puis convertit ces points en valeurs numériques pour former une image numérique.

    !!! success "Question 2"

        Répondez à ce _Questionnaire à Choix Multiple_ (une seule bonne réponse par question).
        
        {{multi_qcm(
["En quelle année a été inventé le premier capteur CCD ?", 
["1900", "1975", "1969", "2007"], [3]],
["Quel est l'effet physique qui crée un déplacement de charges électriques par exposition à la lumière d'un CCD ?",
["L'effet Joule", "L'effet piezoélectrique", "L'effet photoélectrique", "L'effet Doppler"], [3]],
["Quelle partie d'un capteur CCD transforme l'énergie d'un photon en charge électrique ? ",
["Un transistor", "Un capteur de charge",  "Un objectif", "Un photosite"], [4]],
["Le déplacement de charges généré par un photosite :  ",
["Peut être traité directement par un algorithme de traitement d'image", "Doit d'abord être converti d'analogique en numérique par un CAN avant d'être traité par un algorithme de traitement d'image"], [2]]
)}}


!!! note "Point de cours 4"
    === "Capteur CCD"
        Le composant de base d'un appareil de photographie numérique est le **capteur CCD**   (_Charge Coupled Device_) , inventé en 1969. Il  capture la lumière à travers des **photosites** (diamètre de l'ordre du micromètre), qui la convertissent en charges électriques avec **l'effet photoélectique**. Ces charges sont transférées jusqu'à un **convertisseur analogique/numérique**  qui génère un signal numérique (séquence de 0 et de 1) pour créer une image numérique.

        [Diaporama de présentation des capteurs CCD](./ressources/diaporama_capteur_ccd.html)

        ![alt](https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Webcam_CCD_-_640x480px_Colour.jpg/640px-Webcam_CCD_-_640x480px_Colour.jpg){:.center}

        > _Source : Le capteur CCD d'une webcam. Wikipedia (Zephyris)_

    === "Format RAW"
        Après la capture par le capteur CCD, un fichier au format [RAW](https://fr.wikipedia.org/wiki/RAW_%28format_d%27image%29){:target="_blank"}  est produit. Le fichier contient les données brutes enregistrées par le capteur et les métadonnées qui permettent d'en tirer une image. Sur les appareils bas de gamme, les données sont immédiatement modifiées et enregistrées dans un autre format compressé comme JPEG en appliquant des traitements de base (correction des distorsions de l'objectif, balance des blancs, contraste). Les appareils haut de gamme fournissent le fichier [RAW](https://fr.wikipedia.org/wiki/RAW_%28format_d%27image%29){:target="_blank"}  qui permet de personnaliser le traitement d'image.

    === "Importance des algorithmes"
        Les algorithmes jouent un rôle crucial dans la photographie numérique  et sont appliqués avant la prise de vue ou sur les données brutes au format [RAW](https://fr.wikipedia.org/wiki/RAW_%28format_d%27image%29){:target="_blank"}   fournies par le  capteur CCD :

        - **Prise de vue** :  
            - Calcul de l'exposition
            - Mise au point automatique
            - Stabilisation d'image
        - **Développement** :- 
            - Balance des blancs
            - Correction des distorsions
            - Ajustement de la netteté et du contraste
        - **Compression de fichiers** :
            - Compression sans perte (ex. : format TIFF)
            - Compression avec perte (ex. : format JPEG)
  
    
