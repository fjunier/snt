---
author: Frédéric Junier
title: Données et informations
---

# Données et informations

!!! abstract "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le parcours thématique Photographie numérique proposé par l'association [France IOI](https://www.france-ioi.org/) sur le site [https://parcours.algorea.org](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025/)
    * le cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/) 
    * des TP sur Capytale de David Landry et Germain Becker.

## Image pixellisée

!!! question "Exercice 1"
    > **Objectif :** _Découvrir la représentation d'une image sous la forme d'une grille de pixels_

    1. Se connecter à son compte sur [parcours-algorea.org](https://login.france-ioi.org/auth){:target="_blank"}.
    2. Dans le  parcours thématique _Photographie numérique_, faire l'exercice [le poisson de Castor](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025-1089626022569595539-546648431660676093/){:target="_blank"}.
    3. Compléter le texte à trous du [Quiz image pixellisée](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025-1089626022569595539-349914841333683613/){:target="_blank"}.

!!! note "Point de cours 1"

    === "Image bitmap"

        Pour représenter numériquement une image on la découpe en une **grille** ou **matrice** de **pixels** (_picture element_) et on associe à chaque pixel une valeur numérique. On parle de **représentation bitmap** (_matrice de bits_), les bits permettant de stocker les valeurs des pixels.

        !!! example "une image en noir et blanc"
            Ci-dessous la représentation en noir et blanc d'une image de poisson dans une matrice de pixels. Chaque pixel vaut 1 s'il est _blanc_ ou 0 s'il est _noir_.

            ![alt](https://static-items.algorea.org/files/checkouts/02a3e908c759556ec457ca36b990bc68/digital_photo/structuration_digital_photo_1/fish.png)

            > _Source :_ [https://parcours.algorea.org/](https://parcours.algorea.org/fr/a/1089626022569595539;p=4702,1067253748629066205,1625996270397195025;pa=0)

    === "Définition et résolution d'une image"
        
        La **définition** d'une image est le nombre de pixels qui composent l'image.

        La **résolution** d'une image est le nombre de pixels par unité de longueur. On l'exprime en général en ppp(_pixels par pouce_)  ou _dots per inch (dpi)_ en anglais, le pouce est une unité de longueur anglo-saxonne mesurant $2,54$ cm. La résolution standard pour le Web est de 72 ppp et pour une impression de 300 ppp. 

        ![alt](https://static-items.algorea.org/files/checkouts/02a3e908c759556ec457ca36b990bc68/digital_photo/structuration_digital_photo_1/size.png)

        > _Source :_ [https://parcours.algorea.org/](https://parcours.algorea.org/fr/a/1089626022569595539;p=4702,1067253748629066205,1625996270397195025;pa=0)


        !!! example "Calcul de la définition et de la résolution d'une image"
            L'image précédente comporte 12 pixels en largeur et 5 pixels en hauteur donc sa définition est de $5 \times 12 = 60$ pixels.
            Cette image a une faible résolution de 3 pixels par pouce.



!!! note "Point de cours 2"

    === "Unités d'information"
        En informatique, l'unité d'information de base est le **bit** (_binary digit_) qui permet de stocker 2 informations (codées par 0 ou 1).
        Un **octet** (_byte_ qui signifie morceau en anglais) est une séquence de 8 bits et permet de stocker $2^{8}=256$ informations (codées par une séquence de 8 valeurs 0 ou 1).

    === "Profondeur d'une image bitmap"
        La **profondeur** d'une image numérique bitmap, est le nombre de bits utilisé par chaque pixel pour coder sa couleur.

        Avec une profondeur de $p$ bits on peut coder $2^{p}$ couleurs différentes.

    === "Poids d'une image"

        A partir de la **définition** et de la **pronfondeur** d'une image bitmap on peut calculer son poids avec la formule : **Poids $=$ Définition $\times$ Profondeur**.

        
        !!! example "Calcul du poids d'une image bitmap"

            Pour une image en noir et blanc, un pixel a deux couleurs possibles. Comme $2=2^{1}$,  il suffit de 1 bit pour coder sa couleur, sa profondeur est de 1 bit.

            Par exemple l'image noir et blanc ci-dessous de définition $12 \times 5 = 60$ et de profondeur $1$ bit a pour poids $60 \times 1= 60$ bits.

            ![alt](https://static-items.algorea.org/files/checkouts/02a3e908c759556ec457ca36b990bc68/digital_photo/structuration_digital_photo_1/size.png)

            > _Source :_ [https://parcours.algorea.org/](https://parcours.algorea.org/fr/a/1089626022569595539;p=4702,1067253748629066205,1625996270397195025;pa=0)

    === "Différentes profondeurs d'images"

        La profondeur d'une image peut varier selon l'échelle des couleurs choisies :

        * pour une image en _noir et blanc_ une profondeur de 1 bit suffit pour représenter $2^{1}$ couleurs
        * pour une image en _niveaux de gris_ une profondeur de 8 bits soit 1 octet  permet de  représenter $2^{8}=256$ nuances de gris. La valeur du pixel représente l'intensité lumineuse donc les nuances de gris s'échelonnent  du noir ($0$) au blanc ($255$)
        * pour une image en _couleurs_ on utilise la **synthèse additive** des couleurs en représentant chaque couleur comme l'addition de trois intensités lumineuses de *Rouge*, de *Vert* et de *Bleu*. La couleur de chaque pixel est donc codée par un triplet **(R, V, B)**. Si chaque composante de ce triplet est codée sur $8$ bits soit $1$ octet cela donne une profondeur de $8 \times 3 = 24$ bits qui permet de représenter $2^{24} \approx 16 \times 10^{6}$ couleurs. 

        !!! example "Quelques calculs"

            |Echelle de couleurs|Profondeur|Poids d'une image de définition $800 \times 600$ en bits|Poids en octets|
            |:---:|:---:|:---:|:---:|
            |noir et blanc|1|$800 \times 600 = 480 000$ bits|$480000/8=60000$ octets|
            |niveaux de gris|8|$800 \times 600 \times 8= 3840000$ bits|$480000$ octets soit $480$ kilooctets (ko)|
            |couleurs|24|$800 \times 600 \times 24= 11520000$ bits|$480000 \times 3 = 1440000$ octets soit $1,44$ megaoctets (Mo)|


        ![alt](https://cgouygou.github.io/2SNT/Cours/images/chromato.jpg){:.center}

        > _Source :   cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/)_

        > Exemples de codages (R, G, B) de pixels d'une image en couleur. Une couleur dont les 3 composantes sont identiques correspond à un niveau de gris.

!!! question "Exercice 2"
    > **Objectif :** _Manipuler les unités d'informations et leurs multiples et sous-multiples_

    Pour chaque question de ce QCM, plusieurs bonnes réponses sont possibles.

    {{multi_qcm(
["1 kilooctets représente", 
["1000 bits", "8000 bits", "$\\frac{1000}{8}$ bits", "$10^{3}$ octets"], [2,4]],
["1 megaoctet représente :",
["1000 octets", "$8 \\times 10^{6}$ bits", "$\\frac{1}{8}$ megabits", "$10^{3}$ kilooctets"], [2,4]],
["Une image de définition 2 megapixels et de profondeur 24 bits a un poids de : ", 
["48 megabits", "6 megaoctets", "6 kilooctets", "$2^{24}$ bits"], [1,2]]
)}}

!!! question "Exercice 3"
    > **Objectif :** _Manipuler la représentation (R,V,B) des couleurs_

    1. Se connecter à son compte sur [parcours-algorea.org](https://login.france-ioi.org/auth).
    2. Dans le  parcours thématique _Photographie numérique_, faire l'exercice [Ambiance lumineuse](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025-1089626022569595539-1806049690416021493/){:target="_blank"}.
    3.  Compléter le texte à trous du  [Quiz - Codage des couleurs](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025-1089626022569595539-363475139505379105/){:target="_blank"}.


## Formats de fichiers d'images numériques

!!! question "Exercice 4"
    > **Objectif :** _Découvrir des méthodes d'encodage et de compression d'image_

    1. Se connecter à son compte sur [parcours-algorea.org](https://login.france-ioi.org/auth).
    2. Dans le parcours thématique _Photographie numérique_, faire l'exercice [Image encodée, image compressée](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025-1089626022569595539-817625730839546105/){:target="_blank"}.  
    3.  Compléter le texte à trous du  [Quiz - Encodage et compression d'une image](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025-1089626022569595539-1595013240262378229/){:target="_blank"}.


!!! note "Point de cours 3"

    === "Format de fichier"
        Un **format de fichier** d'image numérique est une façon d'encoder les informations de l'image numérique dans un fichier informatique qui est une séquence de bits. 
        On distingue :
        
        * les **fichiers textuels** dont la séquence de bits correspond à une séquence de caractères lisibles par l'être humain
        * les **fichiers binaires** dont la séquence de bits ne correspond pas à une séquence de caractères lisibles.
  
    === "Format de fichier textuel"
        Les images numériques sont stockées en général dans des fichiers binaires car un caractère est codé sur au moins 8 bits mais il existe certains formats textuels à but _"pédagogiques"_.
        
        Le format de fichiers [PBM](https://fr.wikipedia.org/wiki/Portable_pixmap){:target="_blank"} P1 permet de stocker une image bitmap noir et blanc dans un fichier textuel.
        La première ligne du fichier contient le code P1
        La deuxième ligne contient la largeur et la hauteur de l'image en pixels
        La ligne suivante contient la séquence de valeurs des pixels.  Les pixels sont parcourus de haut en bas et de gauche à droite et codés par 0 pour noir et 1 pour blanc.

        !!! example "exemple du format PBM"
            Par exemple l'image ci-dessous peut être représentée dans un fichier textuel par :

            ~~~
            P1
            4 3
            011010010110
            ~~~

            ![alt](https://static-items.algorea.org/files/checkouts/ba3b6a97b6251cc3700542ece57cdd34/digital_photo/structuration_digital_photo_2/target_easy.png){:.center}

            > _Source :_ [https://parcours.algorea.org/](https://parcours.algorea.org/fr/a/1089626022569595539;p=4702,1067253748629066205,1625996270397195025;pa=0)

    === "Format de fichier binaire"
        Il existe plusieurs formats de fichiers binaires pour les images numériques. Ils se distinguent principalement selon trois critères :

        * la **profondeur** de l'image qui est liée au  poids de l'image pour une définition fixée
        * le type d'**algorithme de compression** (*avec ou sans perte d'information*). Une *compression avec perte d'information* peut être acceptable pour des informations non discernables par l'oeil humain et permet un meilleur _taux de compression_ et donc un poids plus faible de l'image numérique
        * le type d'utilisation qui dépend des deux autres critères.

        Chaque format est identifié par son **extension** qui s'ajoute après un point au nom du fichier : par exemple `image.png`.

        | Nom du format et extension | Profondeur (nombre de bits par pixel) | Type de compression (avec ou sans perte) | Exemples d'utilisation                       |
        |---------------|---------------------------------------|------------------------------------------|---------------------------------------------|
        | PNG           | 24 bits (Truecolor), 8 bits (Indexed) | Sans perte                               | Logos, illustrations avec transparence, web |
        | JPEG          | 24 bits (Truecolor)                   | Avec perte                               | Photos haute résolution, web                |
        | BMP           | 1, 4, 8, 16, 24, 32 bits              | Sans perte                               | Captures d'écran, images non compressées    |
        | TIFF          | 1, 8, 24, 48 bits                     | Sans perte ou avec perte                 | Imagerie médicale, archivage, impression    |
        | RAW           | 12 à 16 bits                          | Sans perte                               | Photographie professionnelle                |
        | GIF           | 8 bits                                | Sans perte                               | Animations simples, graphiques web          |

        !!! example "exemple du format PNG"
            On reprend l'exemple de l'image ci-dessous dont la représentation au format de fichier textuel PBM P1 est :

            ~~~
            P1
            4 3
            011010010110
            ~~~

            ![alt](https://static-items.algorea.org/files/checkouts/ba3b6a97b6251cc3700542ece57cdd34/digital_photo/structuration_digital_photo_2/target_easy.png){:.center}

            > _Source :_ [https://parcours.algorea.org/](https://parcours.algorea.org/fr/a/1089626022569595539;p=4702,1067253748629066205,1625996270397195025;pa=0)

            Sa représentation au format binaire PNG n'est plus lisible par l'être humain lorsqu'on traduit la séquence de bits en séquence de caractères :

            ![alt](images/image_png.png){:.center}

    === "Métadonnées d'un fichier"
        Les **métadonnées** d'un fichier d'image numérique sont des informations complémentaires  à la représentation de l'image comme l'origine (date localisation géographique), la création (l'auteur), les caractéristiques (techniques de l'appareil) qui  permettent de mieux comprendre l'image et de la classer dans une collection d'images.  
        Les métadonnées des images numériques prises par les appareils photo numériques suivent en général la norme [EXIF](https://fr.wikipedia.org/wiki/Exchangeable_image_file_format){:target="_blank"}.
        Il faut être prudent dans la manipulation des **métadonnées** qui peuvent contenir des données personnelles.

        ![alt](https://upload.wikimedia.org/wikipedia/commons/c/cf/DigiKam_EXIF_information_screenshot.png){:.center}

        > _Source : Wikipedia [https://fr.wikipedia.org/wiki/Exchangeable_image_file_format](https://fr.wikipedia.org/wiki/Exchangeable_image_file_format)_



!!! question "Exercice 5"
    > **Objectif :** _Extraire les métadonnées d'une image_

    1. Se connecter à son compte sur [parcours-algorea.org](https://login.france-ioi.org/auth).
    2. Dans le parcours thématique _Photographie numérique_, faire l'exercice [Métadonnées d'une photographie](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-1625996270397195025-1680805270915318251-1090721188347086739-29497779091629660/){:target="_blank"}.

        !!! tip "Logiciel de traitement d'images"
            Pour cet exercice, on utilisera le logiciel Gimp qui est installé sur les PC du lycée.
            L'interface est très riche, on a donc souvent besoin d'utiliser la documentation ou des tutoriels.
            L'outil _pipette_ permet de  prélever la valeur d'un pixel, il faut le paramétrer avec l'option _Cible de prélèvement -> Pointer seulement_ , voir la [documentation en ligne](https://docs.gimp.org/fr/gimp-tool-color-picker.html).

    3. Déterminer le code postal de la commune où a été prise la photo dont l'URL est [http://frederic-junier.org/SNT/images/20181230_162625.jpg](http://frederic-junier.org/SNT/images/20181230_162625.jpg){:target="_blank"}. On pourra utiliser l'outil en ligne [https://www.verexif.com/fr/](https://www.verexif.com/fr/){:target="_blank"}   pour éditer les metadonnées de la photo ou le plugin Firefox [https://addons.mozilla.org/fr/firefox/addon/exif-viewer/](https://addons.mozilla.org/fr/firefox/addon/exif-viewer/){:target="_blank"}.