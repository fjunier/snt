---
author: Frédéric Junier
title: Introduction
---


!!! cite "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le [programme de SNT](https://eduscol.education.fr/document/23494/download)
    * le parcours thématique Données strucuturées proposé par l'association [France IOI](https://www.france-ioi.org/) sur le site [https://parcours.algorea.org](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123/)
    * le cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/) 
    * un TP Capytale d'une formation SNT pour l'académie de Rennes


!!! question "Activité d'introduction"

    <iframe width="1580" height="889" src="https://www.youtube.com/embed/IJJgcZ2DEs0" title="MOOC SNT / Données, comment les manipuler ?" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

    !!! success "Question 1"
        Regardez la vidéo précédente.
    
    !!! success "Question 2"
        1. Se créer un compte sur [parcours.algorea.org](https://login.france-ioi.org/register){:target="_blank"} puis se connecter et rejoindre le groupe SNT-Parc-2024 depuis [https://parcours.algorea.org/profile/groupsMember](https://old.parcours.algorea.org/profile/groupsMember){:target="_blank"} avec le code donné dans Pronote ou par le professeur.
        2. Répondre au [Quizz](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1347056385813169871-141668197491955794/){:target="_blank"} sur la vidéo précédente.
