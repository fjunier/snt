---
author: Frédéric Junier
title: Impact sur les pratiques humaines
---

#  Impact sur les pratiques humaines

!!! abstract "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le [programme de SNT](https://eduscol.education.fr/document/23494/download){:target="_blank"}
    * le parcours thématique Données strucuturées proposé par l'association [France IOI](https://www.france-ioi.org/) sur le site [https://parcours.algorea.org](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123/){:target="_blank"}
    * le cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/)
    * un TP Capytale d'une formation SNT pour l'académie de Rennes

!!! note "Point de cours 5"
    === "Big Data"
        L'évolution des capacités de stockage, de traitement et de diffusion  conduit à une explosion du volume de données disponibles. On parle de **Big Data**.

        Ces données massives peuvent être exploitées gràce à l'augmentation de la puissance de calcul et aux progrès des systèmes d'Intelligence Artificielle, dont l'entraînement nécessite également de gros volumes de données.

        <a href="https://fr.statista.com/infographie/17800/big-data-evolution-volume-donnees-numeriques-genere-dans-le-monde/" title="Infographie: Le Big Bang du Big Data | Statista"><img src="https://cdn.statcdn.com/Infographic/images/normal/17800.jpeg" alt="Infographie: Le Big Bang du Big Data | Statista" width="100%" height="auto" style="width: 100%; height: auto !important; max-width:960px;-ms-interpolation-mode: bicubic;"/></a> Source : <a href="https://fr.statista.com/graphique-du-jour/">Statista</a>

    === "Open Data" 
        Certaines de ces données, en particulier celles produites par les services publics, sont ouvertes (**Open Data**) et considérées comme des biens communs. Deux beaux exemples de réutilisation de ces données ouvertes : [Covidtracker](https://covidtracker.fr/) outil de visualisation de la progression de l'épidémie très populaire en 2020-2021 et [Suptracker](https://beta.suptracker.org/) plateforme de visualisation de données sur l'orientation postbac.


        ![alt](http://action-nogent.fr/wp-content/uploads/2014/09/Open-data.jpg)

        > _Source : http://action-nogent.fr_
        
    === "RGPD"
        L'exploitation de données personnelles par  les grandes  plateformes (GAFAM) nécessite un cadre juridique transfrontalier qui commence à se mettre en place  comme le [Réglement Général sur la Protection des Données (RGPD)](https://www.cnil.fr/fr/rgpd-de-quoi-parle-t-on) dans la communauté européenne.

        L'exploitation de données personnelles par des régimes autoritaires peut constituer une menace pour les libertés individuelles, comme par exemple l'utilisation de la reconnaissance faciale à des fins d'évaluation sociale en [RPC](https://fr.wikipedia.org/wiki/Histoire_de_la_r%C3%A9publique_populaire_de_Chine).


        ![alt](https://www.cnil.fr/sites/cnil/files/thumbnails/image/je_suis_une_donnee_personnnelle.jpg){:.center}

        > _Source : CNIL  https://www.cnil.fr/fr/rgpd-de-quoi-parle-t-on_

    === "Impact environnemental"
        Les **centres informatiques (data center)** qui hébergent l'infrastructure de stockage de l'internet mondial, sont responsables de 7 à 15 % des impacts du numérique.  D'après un rapport commandé par le gouvernement à  l'association [Green IT](https://www.greenit.fr/etude-empreinte-environnementale-du-numerique-mondial/) en 2020, l'empreinte environnementale du numérique mondial est environ trois fois celle de la France. Les équipements  des data center sont négligeables en nombre par rapport aux équipements utilisateurs mais ils fonctionnent en continu et doivent être refroidis : leur consommation électrique représente environ 25 % de celle de l'ensemble du numérique.

        L'engouement autour de l'Intelligence Artificielle et la concurrence entre les grandes plateformes comme Microsoft ou Google, se traduit par une consommation effrénée de microprocesseurs spécialisés dont le concepteur numéro 1 [Nvidia](https://fr.wikipedia.org/wiki/Nvidia) est devenu l'une des plus grandes capitalisations boursières du numérique.

        ![alt](https://www.greenit.fr/etude-empreinte-environnementale-du-numerique-mondial/assets/svg/france-greenIT-RGB.svg){: .center}

        ![alt](images/impact.png){: .center}

        > _Source :  Greenit https://www.greenit.fr/etude-empreinte-environnementale-du-numerique-mondial/_


!!! question "Exercice 1"
    >  **Objectif :** _Conscience des traces numériques de nos données personnelles_

    Répondre à ce [Quizz](https://www.quiziniere.com/diffusions/D9W4AX).



!!! question "Exercice 2"
    > **Objectif :** _Utiliser un site de données ouvertes, pour sélectionner et récupérer des données_

    1. Accéder aux jeux de données concernant les résultats à Dijon du référendum de 2005 sur le traité établissant une Constitution pour l'Europe  sur [https://www.data.gouv.fr](https://www.data.gouv.fr/fr/datasets/resultats-du-referendum-de-2005/){:target="_blank"}.
    2. Quelle est la licence de ce jeu de données ?
    3. Quels sont les formats des fichiers de données téléchargeables ?
    4. Sur les 8 critères de qualité des métadonnées combien sont remplis ?
    5. Explorer le fichier sur [https://explore.data.gouv.fr](https://explore.data.gouv.fr/fr/datasets/662156ecf479194e3436e16c/#/resources/8183ee3c-af63-420b-be7b-c436a0dc32d2){:target="_blank"} : 

        * Quel est le code du bureau de vote _ECOLE VARENNES MATERNELLE_ ?
        * Combien de procurations ont-été utilisées dans ce bureau de vote ?
        * Combien de votes OUI et de votes NON ont été exprimés dans ce bureau de vote ?
