---
author: Frédéric Junier
title: Machines
---

# Machines


!!! abstract "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le [programme de SNT](https://eduscol.education.fr/document/23494/download)
    * le parcours thématique Données strucuturées proposé par l'association [France IOI](https://www.france-ioi.org/) sur le site [https://parcours.algorea.org](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123/)
    * le cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/) 
    * un TP Capytale d'une formation SNT pour l'académie de Rennes




## Histoire des supports de stockage


!!! note "Point de cours 3"
    === "Bandes magnétiques (1950)"
        Les **bandes magnétiques** utilisent une bande de matériau magnétisable pour enregistrer les données de manière séquentielle, c'est-à-dire que pour atteindre une donnée il faut parcourir toutes celles stockées précédemment.

        ![alt](https://upload.wikimedia.org/wikipedia/commons/6/6c/IBM_729_Tape_Drives.nasa.jpg){:.center}

        > _Source :  Bandes magnétiques IBM 729, Wikipedia_

    === "Disquettes (années 1970)"
        Les **disquettes** sont des supports de stockage magnétique portables avec des capacités limitées, utilisées pour le transfert et le stockage de données sur les premiers ordinateurs personnels.

        ![alt](https://upload.wikimedia.org/wikipedia/commons/f/fa/Floppy_disk_internal_diagram.svg){:.center}

        > _Source :Fastfission, Public domain, via Wikimedia Commons_


        |Numéro|Légende|
        |---|---|
        |1|Indication de la taille de la disquette (en face : volet de protection en écriture)|
        |2|Hub|
        |3|Cache|
        |4|Habillage en plastique|
        |5|Anneau de papier|
        |6|Disque magnétique|
        |7|Secteur du disque|

   

    === "Disques durs (années 1980)"
        Inventé en 1956, le **disque dur**  utilise des plateaux rigides recouverts de matériau magnétique pour stocker les données. Offrant de grandes capacités et des temps d'accès rapides, les disques durs accompagnent la démocratisation de l'informatique dans les   années 1980  et deviennent  le principal support de stockage interne pour les ordinateurs.


        ![alt](https://upload.wikimedia.org/wikipedia/commons/0/01/Hard_drive-fr.svg){:.center}

        > _Source : I.Surachit, Wikimedia Commons_


    === "SSD (années 2000)"
        Les **mémoires  SSD** utilisent une technologie de mémoire flash à base de transistors, pour stocker les données. Elles offrent des vitesses de lecture/écriture supérieures à celles des disques magnétiques et  une faible consommation d'énergie. Elles  deviennent courantes dans les ordinateurs et appareils mobiles.


        ![alt](https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/2023_Dysk_SSD_Kingston_NV2_2TB.jpg/640px-2023_Dysk_SSD_Kingston_NV2_2TB.jpg)
    
        > _Source : mémoire SSD de 2 TO (2023), Wikimedia Commons_


    === "Stockage en ligne (années 2010)"
        Le **stockage en ligne**, ou "dans le nuage", permet de stocker des données sur des serveurs distants accessibles via Internet, offrant une capacité évolutive et un accès global, facilitant la collaboration et les sauvegardes automatisées.

        ![alt](https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Nuage33.png/699px-Nuage33.png?20131218190024)


        > _Source : Sam Johnston, Wikimedia Commons_


!!! question "Exercice 1"
    > **Objectif :** _Identifier les principaux supports de stockage en informatique_

    Pour chaque question de ce QCM, plusieurs bonnes réponses sont possibles.

    {{multi_qcm(
    ["Quelle technologie de stockage utilise un support magnétique mobile   pour lire ou enregistrer des données ?", 
    ["Un disque dur à plateau", "Une disquette", "Une clef USB", "Des bandes magnétiques"], [1, 2, 4]],
    ["Quelles sont les principales caractéristiques des SSD (Solid State Drives) par rapport aux disques durs traditionnels ?",
    ["Ils utilisent des plateaux rigides", "Ils utilisent de la mémoire flash sans pièces mobiles", "Ils  lisent et écrivent des données de manière séquentielle", "Ils sont moins sensibles aux vibrations"], [2, 4]],
    ["Quelle entreprise est leader mondial dans le secteur du cloud computing d'entreprise ?", 
    ["Google", "Baidu", "Microsoft", "Amazon"], [4]]
    )}}
