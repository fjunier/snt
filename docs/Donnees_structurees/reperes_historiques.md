---
author: Frédéric Junier
title: Repères historiques
---


!!! abstract "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le [programme de SNT](https://eduscol.education.fr/document/23494/download)
    * le parcours thématique Données strucuturées proposé par l'association [France IOI](https://www.france-ioi.org/) sur le site [https://parcours.algorea.org](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123/)
    * le cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/) 
    * un TP Capytale d'une formation SNT pour l'académie de Rennes



!!! note "Point de cours 1 : repères historiques"

    <iframe src='https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=1-nHbQ8eq-hzz-ZPy8g3I0y50qintMOm7CvONSnPb51I&font=Default&lang=en&initial_zoom=2&height=650' width='100%' height='650' webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder='0'></iframe>

