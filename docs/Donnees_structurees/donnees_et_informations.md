---
author: Frédéric Junier
title: Données et informations
---

# Données et informations

!!! abstract "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le [programme de SNT](https://eduscol.education.fr/document/23494/download)
    * le parcours thématique Données strucuturées proposé par l'association [France IOI](https://www.france-ioi.org/) sur le site [https://parcours.algorea.org](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123/)
    * le cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/) 
    * un TP Capytale d'une formation SNT pour l'académie de Rennes

## Données structurées

!!! question "Exercice 1"
    > **Objectif :** _Réaliser des opérations de recherche (filtre) sur une table_

    1. Se connecter à son compte sur [parcours-algorea.org](https://login.france-ioi.org/auth){:target="_blank"}.
    2. Dans le parcours thématique _Données structurées_, faire l'exercice  [Je choisis](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1347056385813169871-1833838287194935391/){:target="_blank"}.
    3. Compléter le texte à trous du [Quiz un filtre](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1347056385813169871-1578790432904769437/){:target="_blank"} 

!!! note "Point de cours 1"

    === "Données"
        Une **donnée** est une valeur décrivant un objet,  une personne, un événement digne d'intérêt pour celui qui choisit de la conserver. 

        !!! example "Exemple"
            La chanson [She Belongs to Me](https://en.wikipedia.org/wiki/She_Belongs_to_Me) peut être décrite par plusieurs données :
            
            |Titre|Auteur|Durée (en secondes)|Année|Album|
            |---|---|----|---|---|
            |'She Belongs to Me'|'Bob Dylan'|170|1965|' Bringing It All Back Home'|
 

    === "Descripteurs"
        Si on veut gérer une collection de chansons, on a besoin de décrire chaque objet chanson par les mêmes catégories de données qu'on appelle des **descripteurs**. La valeur d'un descripteur est une donnée et doit appartenir au domaine qui caractérise le descripteur, on peut ainsi comparer les données de deux chansons (leur année par exemple).
       
        !!! example "Exemple"
            On peut organiser une collection de chansons décrites avec cinq descripteurs. 
            
            |Decripteur 1 : titre|Decripteur 2 : auteur|Decripteur 3 : durée (en secondes)|Descripteur 4 : année|Descripteur 5 : album|
            |---|---|---|---|---|
            |Domaine : chaîne de caractères|Domaine : chaîne de caractères|Domaine : entiers|Domaine : entiers|Domaine : chaîne de caractères|
            |'She Belongs to Me'|'Bob Dylan'|170|1965|'Bringing It All Back Home'|
            |'Under the Bridge'|'Red Hot Chili Pepper'|284|1991|'Blood Sugar Sex Magik'|

    === "Données structurées"
        Une collection regroupe des objets partageant les mêmes descripteurs (par exemple, une collection de chansons). La structure de **table** permet de présenter une collection : les objets en ligne, les  descripteurs en colonne et les données à  l'intersection. Les données sont alors dites **structurées**.

        ![alt](https://cgouygou.github.io/2SNT/Cours/images/table_donnee.png)

        > _Source :   cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/01-Data/)_

    === "Information"
        Des données structurées peuvent être interprétées pour en extraire de l'information.  **L'informatique** est la science du traitement automatique de l'information. 

        !!! example "exemple"
            Avec un filtre sur les valeurs du descripteur _Couleur_ on peut extraire de la table ci-dessous tous les végétaux de couleur _verte_ :


            ![alt](https://static-items.algorea.org/files/checkouts/d0d3ab6da37f0b03865762152adf9241/structured_data/structuration_filter/diagram_filter.png)

            > _Source :_ [https://parcours.algorea.org/](https://parcours.algorea.org/fr/a/1347056385813169871;p=4702,1067253748629066205,653650670442840123;a=0)


!!! question "Exercice 2"
    > **Objectif :** _Réaliser des opérations de recherche (filtre) sur une table_

    1. Se connecter à son compte sur [parcours-algorea.org](https://login.france-ioi.org/auth){:target="_blank"}.
    2. Dans le  parcours thématique _Données structurées_, faire l'exercice [Non, pas ceux là !](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1347056385813169871-951982980587093939/){:target="_blank"}.


!!! question "Exercice 3"
    > **Objectif :** _Réaliser des opérations de recherche (filtre) et de tri sur une table_

    1. Se connecter à son compte sur [parcours-algorea.org](https://login.france-ioi.org/auth){:target="_blank"}.
    2.  Dans le parcours thématique _Données structurées_, faire l'exercice [Dans un certain ordre](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1347056385813169871-1792119325871572520/){:target="_blank"}
    3.  Compléter le texte à trous du  [Quiz - Un tri](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1347056385813169871-188624475356610617/){:target="_blank"}.
    4. Dans le parcours thématique _Données structurées_, faire l'exercice [Ça et ça, ça ou ça !](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1347056385813169871-568936785726181442/){:target="_blank"}.


## Formats de fichiers 

!!! note "Point de cours 2"

    === "Format de fichier"
        Pour assurer leur durabilité et leur partage, les données sont conservées sous différents **formats de fichiers**.

        !!! example "Format CSV"
            Le format de fichier [CSV (Comma-Separated Values)](https://fr.wikipedia.org/wiki/Comma-separated_values){:target="_blank"} est un format de fichier texte utilisé pour enregistrer des données structurées en table :  

            *   chaque titre de colonne correspond à un  **descripteur**
            *   chaque ligne contient l'enregistrement des  données d'un objet : les valeurs pour chaque descripteur sont séparées par un symbole (virgule, point virgule ou tabulation selon le type de format CSV). 

            Un exemple de table avec deux descripteurs, trois enregistrements et le symbole  ',' comme séparateur :

            ~~~
            Nom,Prénom,Âge
            Yamal,Lamine,17
            Mbappé,Kylian,25                      
            Foden,Phil,24
            ~~~

    === "Format ouvert"
        Un format de données est considéré comme **ouvert** (ou libre) si son mode d'organisation est rendu public par son créateur et qu'aucune restriction légale (telles que le droit d'auteur, les brevets ou le copyright) n'empêche son utilisation libre.

    === "Interopérabilité"
        Les formats ouverts sont souvent conçus pour favoriser **l'interopérabilité** : un document sauvegardé dans un format ouvert ne dépend pas du logiciel utilisé pour le créer, le modifier ou le lire. Ainsi, l'utilisateur peut choisir librement le logiciel qu'il souhaite utiliser.


    === "Métadonnées"
        Les **métadonnées** d'un fichier sont des informations descriptives sur les données contenues dans ce fichier, telles que le titre, l'auteur, la date de création ou le format. Elles facilitent la recherche, la gestion et l'utilisation des données.

        !!! example "Exemple"
            Par exemple le jeu de données des [Effectifs délèves en lycée d'enseignement général et technologique sur https://www.data.gouv.fr/](https://www.data.gouv.fr/fr/datasets/effectifs-deleves-par-niveau-sexe-langues-vivantes-1-et-2-les-plus-frequentes-par-lycee-denseignement-general-et-technologique-date-dobservation-au-debut-du-mois-doctobre-chaque-annee/){:target="_blank"}  comporte deux fichiers dont un au format [CSV](https://fr.wikipedia.org/wiki/Comma-separated_values){:target="_blank"} et l'autre au format [JSON](https://fr.wikipedia.org/wiki/JavaScript_Object_Notation){:target="_blank"}. Les **métadonnées** de ces fichiers portent sur les URL d'accès, la description du contenu, les dates de création et de modification ... 


            ![alt](images/metadonnees.png){:.center}


 


!!! question "Exercice 4"

    > **Objectif :** _Identifier les principaux formats et représentations de données_

    Pour chaque question de ce QCM, plusieurs bonnes réponses sont possibles.

    {{multi_qcm(
["Que représente le titre d'une colonne dans un fichier CSV ?", 
["La valeur d'une donnée", "Un descripteur", "Une métadonnée", "Un commentaire"], [2]],
["Une suite bureautique permettant de créer des fichiers en format ouvert est :",
["Microsoft Office", "Libre Office", "Only Office", "Collabora"], [2, 3, 4]],
["Un fichier CSV est : ", 
["un fichier textuel lisible par un humain", "un fichier binaire illisible par un humain", "un format des débuts de l'informatique qui n'est plus utilisé", "lisible par un logiciel tableur"], [1, 4]]
)}}


!!! question "Exercice 5"

    > **Objectif :** _Identifier les principaux formats et représentations de données_

    1. Se connecter à son compte sur [parcours-algorea.org](https://login.france-ioi.org/auth){:target="_blank"}.
    2. Dans le parcours thématique _Données structurées_ : _Progresser et Valider / Format de données_, lire la présentation du format  [CSV](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-169388080788275719-1683014833904735819/){:target="_blank"}.
    3. Dans le parcours thématique _Données structurées_ : _Progresser et Valider / Format de données_, faire l'exercice [Reproduire un CSV](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-169388080788275719-885629376711585074/){:target="_blank"}.
    4. Faire l'exercice [Dupliquer une table](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-169388080788275719-1205560747811800655/){:target="_blank"} sauf la version ⭐⭐⭐⭐. 

!!! question "Exercice 6"

    Le format de fichier *vCard* est un format standard ouvert d'échange de données personnelles au format de carnet d'adresses qui peut contenir une ou plusieurs adresses. Lorsqu'un fichier *vCard* ne contient qu'une seule adresse, nous parlons plus naturellement de carte de visite virtuelle.


    ??? info "Norme du format vCard"
        La norme du format vCard version 4.0 est décrite dans [l'article Wikipedia](https://fr.wikipedia.org/wiki/VCard){:target="_blank"} :

        > Toute vCard commence par BEGIN:VCARD et se termine par END:VCARD. Toute vCard doit contenir :
        > * la propriété VERSION, qui définit la version de la vCard. VERSION doit apparaître immédiatement après BEGIN. 
        > * la propriété N: avec des champs textuels séparés par un ;   le nom, le prénom les titres etc ... 

    Voici un exemple de fichier [VvCard](https://fr.wikipedia.org/wiki/VCard){:target="_blank"} :

    ~~~
    BEGIN:VCARD
    VERSION:4.0
    FN: Evariste Galois
    N: Galois;Evariste;;Pr;
    EMAIL: evariste.galois@ens-ulm.fr
    TEL:06 31 41 59 26
    ADR:;;45 rue d'Ulm;Paris;;75005;France
    BDAY:1970-10-25
    END:VCARD
    ~~~

    1. Donner la définition d'un format de fichier ouvert.
    2. Indiquer quelle est l'extension d'un fichier vCard.
    3. Avec l'éditeur de textes **Notepad++**,   créer un fichier  au format vCard avec la carte de visite imaginaire d'un homonyme d'une  figure de l'informatique choisie parmi les 7 familles de l'informatique :

        * [Famille Mathématiques et Informatique](https://interstices.info/famille-mathematiques-informatique/){:target="_blank"}
        * [Famille Algorithmique et programmation](https://interstices.info/famille-algorithmes-programmation/){:target="_blank"}
        * [Famille Sécurité et confidentialité](https://interstices.info/famille-securite-confidentialite/){:target="_blank"}
        * [Famille Système et Réseaux](https://interstices.info/famille-systemes-reseaux/){:target="_blank"}
        * [Famille Machines et Composants](https://interstices.info/famille-machines-composants/){:target="_blank"}
        * [Famille Intelligence Artificielle](https://interstices.info/famille-intelligence-artificielle/){:target="_blank"}
        * [Famille Interaction Homme-Machine](https://interstices.info/famille-interaction-homme-machine/){:target="_blank"}
    4. Importer ce fichier vCard comme nouveau contact sur son Webmail.


