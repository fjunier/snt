---
author: Frédéric Junier
title: Algorithmes et programmes
---

# Algorithmes et programmes

!!! abstract "Sources et crédits pour ce cours"
    Pour préparer ce cours, j'ai utilisé :

    * le [programme de SNT](https://eduscol.education.fr/document/23494/download)
    * le parcours thématique Données strucuturées proposé par l'association [France IOI](https://www.france-ioi.org/) sur le site [https://parcours.algorea.org](https://parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123/)
    * le cours de [Cédric Gouygou](https://cgouygou.github.io/2SNT/Cours/02-Photo/) 
    * un TP Capytale d'une formation SNT pour l'académie de Rennes




## Recherche dans une collection de données

!!! note "Point de cours 4"
    
    === "Recherche dans une table"
        Une table de données structurées  peut faire l'objet de différentes opérations : 
        
        * rechercher une information précise dans la collection
        * trier la collection selon un ou plusieurs descripteurs
        * filtrer la collection selon un ou plusieurs tests sur les valeurs des descripteurs
        *  effectuer des calculs
        *  mettre en forme les informations produites pour une visualisation par les utilisateurs.

    === "Recherche dans une base de données"
        Une **base de données** est une collection de plusieurs tables de données structurées qu'on peut rapprocher sur les valeurs de descripteurs communs pour générer de nouvelles informations. 

        Les *Systèmes de Gestion de Bases de Données (SGBD)* sont très utilisés pour gérer les données structurées : abonnés d'une plateforme, stocks et commandes, systèmes de réservation ...

        Les recherches dans une base de données sont effectuées à l'aide de **requêtes**.

        ![alt](images/sgbd_client_serveur.png){:.center}

        ??? info "Modèle relationnel"
            Dans le [modèle relationnel](https://fr.wikipedia.org/wiki/Base_de_donn%C3%A9es_relationnelle) les tables sont appelées *relations* et les descripteurs *attributs* : 

            * une *clef primaire* est un attribut ou un ensemble d'attributs identifiant de façon unique un enregistrement ($=$ une ligne) de la relation ($=$ table)
            * une *clef étrangère* est un attribut ou un ensemble d'attributs qui fait référence à une clef primaire d'une autre relation et permet de faire la jointure entre deux relations pour générer de nouvelles informations.

            Le langage [SQL](https://fr.wikipedia.org/wiki/Structured_Query_Language) permet d'effectuer des requêtes sur une base de données relationnelles.
        
            ![alt](images/bdd2.png){:.center}
    

        !!! example "Exemple de la gestion d'une base de données musicale"
            Pour gérer une base de données de morceaux de musique, il est préférable d'utiliser plusieurs tables plutôt qu'une seule. Séparer les données des morceaux de celles des interprètes permet de ne pas ressaisir le nom de l'interprète pour chaque morceau. En éliminant les redondances, on réduit les risques d'erreurs de saisie ou de modification.

            ![alt](images/bdd.png){:.center}


    === "Recherche dans des données non structurées"
        Certains fichiers contiennent des données non structurées, par exemple des images, des documents de traitement de texte ou PDF. **L'indexation** permet de leur associer des **métadonnées** (titre, date) ou des **mots clefs** qui sont ensuite utilisés pour la recherche dans une collection de documents. Cette **indexation** peut être manuelle ou automatisée à l'aide de programmes de reconnaissance automatique de caractère (OCR) ou qui vont lire les métadonnées déjà présentes (dans les images par exemple ...)

        Les logiciels  de *Gestion Electronique de Données (GED)*, comme [Microsoft Sharepoint](https://fr.wikipedia.org/wiki/Microsoft_SharePoint) permettent de gérer des collections de données non structurées (factures dans une entreprise, archives  ...)
    

!!! question "Exercice 1"
    > **Objectif :** _Connaître les principales méthodes de recherche dans une collection de   données_

    Pour chaque question de ce QCM, plusieurs bonnes réponses sont possibles.

    {{multi_qcm(
    ["Que gère un Système de Gestion de Bases de Données ?", 
    ["Une collection de plusieurs tables de données structurées", "Une collection de documents non structurés", "Un fichier CSV", "Un dossier avec des sous-dossiers par catégories de fichiers"], [1]],
    ["Comment les recherches dans une base de données sont-elles effectuées ?",
    ["Par tri manuel des tables", "À l'aide de requêtes", "Par classement alphabétique", "Par reconnaissance automatique de caractères (OCR)"], [2]],
    ["Quelle est la fonction principale des logiciels de Gestion Électronique de Données (GED) ?", 
    ["Gérer des tables de données structurées", "Gérer des collections de données non structurées", "Trier les commandes et les stocks", "Effectuer des calculs sur des données structurées"], [2]],
    ["Quel est le nom du langage de requête des Systèmes de Gestion de Bases de Données ?", 
    ["CSV", "OCR", "XLS", "SQL"], [4]]
    )}}

## Utiliser un logiciel de tableur pour rechercher dans une table



!!! question "Exercice 2"
     <iframe width="766" height="430" src="https://www.youtube.com/embed/0_ri8yr6a-8" title="Le Tableur" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

    > **Objectif :** _Réaliser des opérations de recherche, filtre, tri ou calcul sur une ou plusieurs tables, à l'aide d'un tableur_

    !!! success "Question 1"
        Regardez la vidéo précédente pour découvrir la façon dont un logiciel de tableur permet de gérer une table de données structurées.  


    !!! success "Question 2"
        1. Se connecter à son compte sur [parcours-algorea.org](https://login.france-ioi.org/auth){:target="_blank"}.
        2. Lire attentivement la page [À consulter absolument avant de commencer ! ](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-144438975691201992-903647551153998039/){:target="_blank"} : télécharger le fichier [musiques.csv](https://static-items.algorea.org/files/checkouts/a97b14dbb1f5bf408ec6f2c112907e75/structured_data/music_presentation/musiques.csv){:target="_blank"} et ouvir le fichier avec le tableur **LibreOffice** en suivant les instructions. 
        3. Traiter  les six exercices _Musique 1_ à _Musique 6_ dont les liens d'accès se trouvent sur la page [Utiliser un tableur](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-144438975691201992/){:target="_blank"}. Chaque exercice est une série de petits défis portant sur des recherches et tris à effectuer sur le fichier [musiques.csv](https://static-items.algorea.org/files/checkouts/a97b14dbb1f5bf408ec6f2c112907e75/structured_data/music_presentation/musiques.csv){:target="_blank"} avec le tableur **LibreOffice**.




## Utiliser un langage de programmation pour interroger une base de données


!!! question "Exercice 3"

    

    !!! success "Question 1"

        > **Objectif :** _Réaliser des opérations de recherche, filtre, tri ou calcul sur une ou plusieurs tables, à l'aide d'un langage de programmation_


        1. Se connecter à son compte sur [parcours-algorea.org](https://login.france-ioi.org/auth){:target="_blank"}.
        2. Traiter l'exercice [Les régions et leurs capitales](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-896705699396833785-1350493012782573557/){:target="_blank"} dans le module [Programmer des requêtes - Python](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-896705699396833785/){:target="_blank"}
        3. Traiter l'exercice [Les grandes villes](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-896705699396833785-298290967233684619/){:target="_blank"} dans le module [Programmer des requêtes - Python](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-896705699396833785/){:target="_blank"}
        4. Traiter l'exercice [Les fleuves](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-896705699396833785-124649534771233223/){:target="_blank"} dans le module  [Programmer des requêtes - Python](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-896705699396833785/){:target="_blank"}
        5. Traiter l'exercice [La Région Grand-Est](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-896705699396833785-1283270599661722203/){:target="_blank"} dans le module  [Programmer des requêtes - Python](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-896705699396833785/){:target="_blank"}
        6. Traiter l'exercice [Les villes du Pas-de-Calais](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-896705699396833785-1745663858221771364/){:target="_blank"} dans le module  [Programmer des requêtes - Python](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-896705699396833785/){:target="_blank"}
        7. Traiter l'exercice [Les habitants des Hauts-de-Seine](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-896705699396833785-768735074621179907/){:target="_blank"} dans le module  [Programmer des requêtes - Python](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-896705699396833785/){:target="_blank"}
        8. Traiter l'exercice [Les départements de Nouvelle-Aquitaine](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-896705699396833785-94376821983278913/){:target="_blank"} dans le module  [Programmer des requêtes - Python](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-1174881031623574659-896705699396833785/){:target="_blank"}


    !!! success "Question 2 (en route vers la spécialité NSI)"

        > **Objectif :** _À partir de deux tables de données ayant en commun un descripteur, réaliser un croisement des données permettant d'obtenir une nouvelle information._


        1. Se connecter à son compte sur [parcours-algorea.org](https://login.france-ioi.org/auth){:target="_blank"}.
        2. Traiter les deux exercices [Les villes de Bretagne](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-826788778693939268-1428143622185201137-15509555226201106/){:target="_blank"} et [Les capitales de région](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-826788778693939268-1428143622185201137-563467421528066325/){:target="_blank"} de  la page [Programmer des requêtes avancées - Python](https://old.parcours.algorea.org/contents/4707-4702-1067253748629066205-653650670442840123-826788778693939268-1428143622185201137/){:target="_blank"}


!!! question "Exercice 4"

    > _Source : TP de la bibliothèque Capytale, réalisé par une équipe de formation de l'académie de Rennes_


    Pour traiter des données, ce  TP utilise  la bibliothèque Python Pandas. Une bibliothèque Python permet de rajouter des fonctionnalités par rapport au langage de base. La bibliothèque Pandas est  très utilisée pour tout ce qui touche au traitement des données.


    * Traiter le [💻 TP sur Capytale](https://capytale2.ac-paris.fr/web/c/32c9-3801103).
    * Vérifier ses résultats ou se débloquer  avec la [correction](https://capytale2.ac-paris.fr/web/c/d1e3-3798319).




    




    



    