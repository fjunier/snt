---
author: Frédéric Junier
title: Premiers pas
---




!!! tip "Initialisation des connexions"

    !!! success "Connexion au réseau pédagogique"
        
        L'ouverture d'une session sur un poste du __réseau pédagogique__ se fait avec un couple _(identifiant, mot de passe)_.

        Par défaut l'identifiant est au format `prenom.nom` et le mot de passe au format `pNJJ/MM/AAAA` où  `p` est la première lettre du prénom en minuscule, `N` la première lettre du nom en majuscule suivies de la date de naissance.

        Lors de la première connexion, le changement de mot de passe (au moins 8 caractères) est obligatoire. Attendre 10 secondes après le changement de mot de passe, éteindre le PC et le rallumer.

    !!! success "Activation du compte  Educonnect et accès à l'ENT"
        Sur [https://le-parc.ent.auvergnerhonealpes.fr](https://le-parc.ent.auvergnerhonealpes.fr) choisir le profil élève et saisir son identifiant Educonnect et son mot de passe provisoire.

        Une fois le mot de passe  Educonnect modifié, accéder à [l'ENT](https://le-parc.ent.auvergnerhonealpes.fr)


!!! question "Découverte de la messagerie"
    
    !!! success "Fiche d'informations"
        1. Avec Libreoffice Writer, créer une fiche d'informations avec :
            * votre nom, votre prénom, votre classe
            * vos trois matières préférées
            * vos loisirs et centres d'intérêt
            * les métiers qui pourraient vous intéresser
        2. Enregistrer ce fichier dans son espace personnel (raccourci Mes Documents) sur le réseau pédagogique
        3. Exporter ce fichier au format pdf
        4. Importer ce pdf dans son _Porte documents_ sur [l'ENT](https://le-parc.ent.auvergnerhonealpes.fr)
        5. Envoyer ce pdf à M.Junier par la messagerie ENT.


        ![alt](images/partages.png){:.center}

!!! tip "Activer son accès à Pronote et configuer l'application mobile"
    
    !!! success "Fiche d'informations"
        1. Accéder à Pronote depuis l'ENT
        2. Configurer l'application mobile en suivant ce [tutoriel](https://docs.index-education.com/docs_fr/fr-pronote-support-fiche-932-3528-configurer-l-application-mobile.php)

        ![alt](images/accueil_ent.png){:.center} 



!!! question "Découvrir  l'ENT"

    {{multi_qcm(
["Les manuels numériques se trouvent dans la rubrique", 
["Etablissement", "Espace des classes", "E-services", "Ressources numériques"], [4]],
["Un accès abonné est offert pour les journaux",
["L'équipe", "Libération", "Le monde", "Courrier international"], [3,4]],
["Je peux réserver un livre au CDI avec", 
["Cafeyn", "esidoc", "Lire l'actu", "Scholarvox"], [2]],
["Dans la messagerie ENT, je peux écrire", 
["à n'importe quel enseignant du lycée", "seulement à mes enseignants", "au proviseur", "au CPE"], [2, 4]]
)}}


!!! tip "Parcours PIX"
    1. Accéder à l'application PIX dans le mediacentre de [l'ENT](https://le-parc.ent.auvergnerhonealpes.fr) rubrique _Ressources numériques_.
    2. Ouvrir l'application PIX. Si besoin demander à M.Junier de réinitialiser le mot de passe si la connexion à PIX s'est faite au collège avec un idenfiant.
    3. Commencer le parcours de rentrée de seconde de code __HARVEM228__.