---
author: Frédéric Junier
title: 🏡 Accueil
---



Cours de SNT au lycée du Parc en 2024/2025 : thèmes _Données structurées_ et _Informatique embarquée_. Les contenus sont principalement issus de ressources partagées.

Sur [https://frederic-junier.org/](https://frederic-junier.org/) se trouvent mes cours sur les thèmes _Web_, _Internet_ et _Informatique embarquée_.


